<?php

class NextBits_HidePrices_Model_Setup extends Mage_Core_Model_Resource_Setup
{
    /**
     * Creates a new attribute
     *
     * Found this very helpful code on inchoo.net, modified it a little for
     * this application. I gotta say the guys at Inchoo really know Magento
     *
    */
    protected function createAttribute($code, $label, $attribute_type, $product_type)
    {
        $attributeData = array(
            'attribute_code'                => $code,
            'is_global'                     => '1',
            'frontend_input'                => $attribute_type,
            'default_value_text'            => '',
            'default_value_yesno'           => '0',
            'default_value_date'            => '',
            'default_value_textarea'        => '',
            'is_unique'                     => '0',
            'is_required'                   => '0',
            'apply_to'                      => $product_type,
            'is_configurable'               => '0',
            'is_searchable'                 => '0',
            'is_visible_in_advanced_search' => '0',
            'is_comparable'                 => '0',
            'is_used_for_price_rules'       => '0',
            'is_wysiwyg_enabled'            => '0',
            'is_html_allowed_on_front'      => '1',
            'is_visible_on_front'           => '0',
            'used_in_product_listing'       => '0',
            'used_for_sort_by'              => '0',
            'frontend_label'                => array($label),
        );

        $attributeModel = Mage::getModel('catalog/resource_eav_attribute');

        if (!isset($attributeData['is_configurable'])) {

            $attributeData['is_configurable'] = 0;

        }

        if (!isset($attributeData['is_filterable'])) {

            $attributeData['is_filterable'] = 0;

        }

        if (!isset($attributeData['is_filterable_in_search'])) {

            $attributeData['is_filterable_in_search'] = 0;

        }

        if (is_null($attributeModel->getIsUserDefined()) || $attributeModel->getIsUserDefined() != 0) {

            $attributeData['backend_type'] = $attributeModel->getBackendTypeByInput($attributeData['frontend_input']);

        }

        $attributeModel->addData($attributeData);
        $attributeModel->setEntityTypeId(Mage::getModel('eav/entity')
                       ->setType('catalog_product')
                       ->getTypeId());
        $attributeModel->setAttributeSetId(4); //catalog_product
        $attributeModel->setAttributeGroupId(4); //General
        $attributeModel->setIsUserDefined(1);

        try {

            $attributeModel->save();

        } catch (Exception $e) {

            //if it already exists our job is done
        }
    }
}
